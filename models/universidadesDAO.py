from bson.json_util import dumps
import time
import logging
import pymongo
import datetime
class universidadesDAO(object):
	"""docstring for universidadesDAO"""
	def __init__(self, database):
		super(universidadesDAO, self).__init__()
		self.db = database.gerenglob
		self.universidades = self.db.universidadesOPSU
		self.registros = self.db.registros
	
	def cleanUniversities(self):
		registers = self.registros.find()

		for register in registers:
			#print register['universidad'].encode('utf-8').upper().decode('utf-8')
			
			
			match = self.universidades.find({'nombre':{'$regex':register['universidad'].encode("utf-8")},'estado':register['estado'].encode("utf-8")},{'_id':0,'nombre':1})
			list_match = list(match)
			if len(list_match) > 0:
				print list_match
				self.registros.update({'_id':register['_id']},{'$set':{
						'universidad':list_match[0]['nombre'],
						'updated':True
					}},False);

			
		print 'Universities Cleaned'