from bson.json_util import dumps
import time
import logging
import pymongo
class leadersDAO(object):
	"""Object DAO for lideres"""
	
	def __init__(self, database):
		super(leadersDAO, self).__init__()
		self.db = database.gerenglob
		self.leader = self.db.lideres


	def getLeadersCityStatistics(self,congreso,congresoID):
		cursor = self.registers.aggregate([
			{'$match':{'id_evento':int(congresoId),'congreso':str(congreso)}},
			{'$group':{'_id':'$ciudad','cant':{'$sum':'$cant'}}},
			{'$sort':{'cant':-1}}
			])
		
		return dumps(cursor['result'])
	def getLeaderUniversityStatistics(self,congreso,congresoID):
		cursor = self.registers.aggregate([
			{'$match':{'id_evento':int(congresoId),'congreso':str(congreso)}},
			{'$group':{'_id':'$universidad','cant':{'$sum':'$cant'}}},
			{'$sort':{'cant':-1}}
			])
		
		return dumps(cursor['result'])

	def updateLeaders(self,lideres):
		for lider in lideres:
			self.leader.update({'congreso':lider['congreso'],'id_evento':lider['id_evento'],'lider':lider['lider']},lider,upsert = True)
			self.updateLog('Updated leader  '+lider['lider']+" AT: "+time.strftime("%d/%m/%y"))
		print 'Leaders Update Ends...'
	
	def updateLog(self,text):
		LOG_FILENAME = 'data.log'
		logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
		logging.debug(text)
			