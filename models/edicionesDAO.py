from bson.json_util import dumps
import time
import logging
import pymongo
import datetime

class edicionesDAO(object):
	"""docstring for edicionesDAO"""
	def __init__(self, database):
		super(edicionesDAO, self).__init__()
		self.db = database.gerenglob
		self.ediciones = self.db.ediciones

	def updateEdiciones(self,congresos):
		date = datetime.datetime.today()	
		for congreso in congresos:
			self.ediciones.update({'id_evento':congreso['id_evento'],'congreso':congreso['congreso'],'date':datetime.datetime(date.year,date.month,date.day)},{
				'$set':{
					'edicion':congreso['edicion'],
					'n_reservas':congreso['n_reservas'],
					'n_inscritos':congreso['n_inscritos'],
					'n_pagos_completos':congreso['n_pagos_completos'],
					'n_con_habitacion':congreso['n_con_habitacion'],
					'n_acompanantes':congreso['n_acompanantes'],
					'n_pagos_disponibles':congreso['n_pagos_disponibles'],
					'n_registrados':congreso['n_registrados'],
					'total':congreso['total'],
					'fecha_evento':congreso['fecha_evento'],
					'fecha':congreso['fecha'],
					'date':datetime.datetime(date.year,date.month,date.day)
					}
				},True)
		
		print 'Ediciones updated'