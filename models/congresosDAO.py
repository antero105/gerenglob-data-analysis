from bson.json_util import dumps
import time
import logging
import pymongo
import datetime
'''
	Objeto congresosDAO.
'''
class congresosDAO(object):
	"""Object DAO for Congresos"""
	def __init__(self, database):
		super(congresosDAO, self).__init__()
		self.db = database.gerenglob
		self.congresos = self.db.congresos
		self.congresos_test = self.db.congresos_test
		
	def getCongresosNames(self):
		cursor = self.congresos.find({},{'nombre':1})
		return dumps(cursor)
	
	def updateCongresoTest(self,congresos):

		for congreso in congresos:
			self.congresos_test.update({'congreso':congreso['congreso'],'ediciones':{'$nin':[congreso['id_evento']]}},{'$push':{
				'ediciones':congreso['id_evento']
				}},True)
		print 'Congreso Test updated!'

	def getCongresoStatistics(self,congreso):
		cursor = self.congresos.find({'nombre':congreso})
		
		return dumps(cursor)
	'''
		Metodo que limpia y guarda la data.
		Wrangling Method
	'''
	def updateCongreso(self,congresos):
		for index,congreso in enumerate(congresos):	
			if self.congresos.find({'nombre':congreso['congreso']}).count() ==1:
				if self.congresos.find({'nombre':congreso['congreso'],'ediciones.id_evento':congreso['id_evento']}).count() == 1:
					self.congresos.update({'nombre':congreso['congreso'],'ediciones.id_evento':congreso['id_evento']},
						{'$set':{
							'ediciones.$.fecha_evento':congreso['fecha_evento'],
							'ediciones.$.n_acompanantes':congreso['n_acompanantes'],
							'ediciones.$.n_con_habitacion':congreso['n_con_habitacion'],
							'ediciones.$.n_inscritos':congreso['n_inscritos'],
							'ediciones.$.n_pagos_completos':congreso['n_pagos_completos'],
							'ediciones.$.n_pagos_disponibles':congreso['n_pagos_disponibles'],
							'ediciones.$.n_registrados':congreso['n_registrados'],
							'ediciones.$.n_reservas':congreso['n_reservas'],
							'ediciones.$.total':congreso['total']
						}},False,True)	
				else:
					self.congresos.update({'nombre':congreso['congreso']},{'$push':{'ediciones':{
						'id_evento':congreso['id_evento'],
						'edicion':congreso['edicion'],
						'fecha_evento':congreso['fecha_evento'],
						'n_acompanantes':congreso['n_acompanantes'],
						'n_con_habitacion':congreso['n_con_habitacion'],
						'n_inscritos':congreso['n_inscritos'],
						'n_pagos_completos':congreso['n_pagos_completos'],
						'n_pagos_disponibles':congreso['n_pagos_disponibles'],
						'n_registrados':congreso['n_registrados'],
						'n_reservas':congreso['n_reservas'],
						'total':congreso['total']
					}}})
					
			else:
				response = []
				response_json = {
					'nombre':'',
					'ediciones':[]
				}
				tem_array = []
				for index, congreso in enumerate(congresos):
					if congreso['congreso'] not in tem_array:
						tem_array.append(congreso['congreso'])

				for index,item in enumerate(tem_array):
					response_json['nombre'] = item
					for congreso in congresos:			
						if item == congreso['congreso']:
							response_json['ediciones'].append({
								'id_evento':congreso['id_evento'],
								'edicion' : congreso['edicion'],
								'n_reservas':congreso['n_reservas'],
								'n_inscritos':congreso['n_inscritos'],
								'n_pagos_completos':congreso['n_pagos_completos'],
								'n_con_habitacion':congreso['n_con_habitacion'],
								'n_acompanantes':congreso['n_acompanantes'],
								'n_pagos_disponibles':congreso['n_pagos_disponibles'],
								'n_registrados':congreso['n_registrados'],
								'fecha_evento':congreso['fecha_evento'],
								'total':congreso['total']
							})
						if response_json in response:
							pass
						else:
							response.append(response_json)
					response_json = {
					'nombre':'',
					'ediciones':[]
					}
	


	
				for r in response:
					self.congresos.update({'nombre':r['nombre']},r,True)
					updateLog('Updated Congreso :'+congreso['congreso']+', at: '+time.strftime("%d/%m/%y"))
		print 'Congresos Update Ends...'
    	def updateLog(self,text):
			LOG_FILENAME = 'data.log'
			logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
			logging.debug(text)