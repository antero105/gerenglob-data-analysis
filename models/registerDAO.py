from bson.json_util import dumps
import time
import logging
import pymongo
class registerDAO(object):
	"""Object DAO for Registros"""
	def __init__(self, database):
		super(registerDAO, self).__init__()
		self.db = database.gerenglob
		self.registers = self.db.registros
	

	def getRegisterCityStatistics(self,congreso,congresoId):
		#cursor = self.registers.find({'congreso':str(congreso),'id_evento':int(congresoId)})
		cursor = self.registers.aggregate([
			{'$match':{'id_evento':int(congresoId),'congreso':str(congreso)}},
			{'$group':{'_id':'$estado','cant':{'$sum':'$cant'}}},
			{'$sort':{'cant':-1}}
			])
		
		return dumps(cursor['result'])
   	def getRegisterUniversityStatistics(self,congreso,congresoId):
   		cursor = self.registers.aggregate([
   			{'$match':{'id_evento':int(congresoId),'congreso':str(congreso)}},
   			{'$group':{'_id':'$universidad','cant':{'$sum':'$cant'}}},
   			{'$limit':20},
   			{'$sort':{'cant':-1}}
   		])

   		return dumps(cursor['result'])

   	def updateRegister(self,registros):
		for registro in registros:
			registro['universidad'] = registro['universidad'].upper() 
			self.registers.update({'id_evento':registro['id_evento'],'congreso':registro['congreso'],'universidad':registro['universidad'].upper(),'ciudad':registro['ciudad'],'estado':registro['estado']},registro,True,True)
			self.updateLog('Updated Registro:'+registro['congreso']+','+registro['universidad']+', At: '+time.strftime("%d/%m/%y"))
		print 'Registers Update Ends...'
	
	def updateLog(self,text):
		LOG_FILENAME = 'data.log'
		logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
		logging.debug(text)
		