import requests
import pymongo
import io, json
from models.congresosDAO import congresosDAO
from models.registerDAO import registerDAO
from models.leadersDAO import leadersDAO
from models.edicionesDAO import edicionesDAO
from models.universidadesDAO import universidadesDAO
'''
	Objeto que recibe las estadisticas de las base de datos, las limpia y las guarda en una base de datos
	en mongodb
'''
def getStatistics():
	events = requests.get('http://app.gerenglob.com/get_statistics/all/892c888e1054043b02a952886c28fcf7/eventos.json').json()
	records = requests.get('http://app.gerenglob.com/get_statistics/all/892c888e1054043b02a952886c28fcf7/registros.json').json()
	leaders = requests.get('http://app.gerenglob.com/get_statistics/all/892c888e1054043b02a952886c28fcf7/lideres.json').json()
	return events,records,leaders

def updateLog(text):
	LOG_FILENAME = 'data.log'
	logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
	logging.debug(text)

def main():    
	connection_string = "mongodb://localhost"
	connection = pymongo.MongoClient(connection_string)
	congreso = congresosDAO(connection)
	register = registerDAO(connection)
	leaders = leadersDAO(connection)
	ediciones = edicionesDAO(connection)
	universidades = universidadesDAO(connection)

	congresos,registros,lideres = getStatistics()
	#congreso.updateCongresoTest(congresos)
	#ediciones.updateEdiciones(congresos)
	#register.updateRegister(registros)
	#leaders.updateLeaders(lideres)
	universidades.cleanUniversities()




main()
